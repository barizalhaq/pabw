<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('showProfile');
    }

    public function profile()
    {
      return view('pages/profile');
    }

    public function updateProfile()
    {
      return view('auth/update_profile');
    }

    public function update(Request $data)
    {
      $user = $msg = User::find(Auth::user()->id);
      $fileName = time() . '.png';

      $this->validate($data, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        'foto' => 'mimes:jpg,png,jpeg|max:1000'.$user->id,
      ]);

      $user->name = $data->name;
      $user->email = $data->email;
      if(!empty($data->foto))
      {
        $data['foto']->storeAs('public/profile', $fileName);
        $user->foto = $fileName;
      }
      $user->save();

      return redirect('/profile');
    }

    public function destroyMsg()
    {
      $msg = User::find(Auth::user()->id);
      $msg->alert = 0;
      $msg->save();

      return redirect('/profile');
    }
}
