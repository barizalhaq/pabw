<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Quote;

class ArtikelController extends Controller
{
    public function index(Request $request)
    {
      $search = urlencode($request->input('search'));

      if(!empty($search))
        $quotes = Quote::where('title', 'like', '%'.$search.'%')->latest()->paginate(5);
      else
        $quotes = Quote::where('role', 1)->latest()->paginate(5);

      return view('pages/artikel', compact('quotes'));
    }

    public function show($slug)
    {
      $quote = Quote::with('comments.user')->where('slug', $slug)->first();

      if(empty($quote))
        abort(404);

      return view('pages/single_artikel', compact('quote'));
    }

    public function kategori($kategori)
    {
      $quotes = Quote::where('tag', $kategori)->latest()->paginate(5);

      return view('pages/kategori_artikel', compact('quotes'));
    }
}
