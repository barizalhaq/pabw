<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Quote;
use App\Models\QuoteComment;
use App\Models\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $slug)
    {
      $this->validate($request, [
        'komentar' => 'required|string|min:2'
      ]);

      $quote = Quote::where('slug', $slug)->first();

      $comment = QuoteComment::create([
        'isi' => $request->komentar,
        'quote_id' => $quote->id,
        'user_id' => Auth::user()->id,
      ]);

      return redirect('/artikel/' . $slug);
    }

    public function destroy($slug, $id)
    {
      $comment = QuoteComment::findOrFail($id);

      if($comment->isOwner())
        $comment->delete();

      return redirect('/artikel/' . $comment->quote->slug);
    }
}
