<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('edit', 'updateKontributor');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'jenis_kelamin' =>['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'jenis_kelamin' => $data['jenis_kelamin'],
        ]);
    }

    protected function edit()
    {
      return view('auth/jadi_contributor');
    }

    protected function updateKontributor(Request $data)
    {
      $update = User::find(Auth::user()->id);

      $this->validate($data, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users,email,'.$update->id,
        'jenis_kelamin' => 'required',
        'no_hp' => 'required|numeric|digits_between:8,15',
        'ktp' => 'required|mimes:jpg,png,jpeg|max:500',
        'alamat' => 'required|string|min:10',
        'nik' => 'numeric|required',
        'tanggal_lahir' =>'required',
      ]);

      $fileName = time() . '.png';

      $update->name = $data->name;
      $update->email = $data->email;
      $update->role = 0;
      $update->no_hp = $data->no_hp;
      $update->alamat = $data->alamat;
      $update->jenis_kelamin = $data->jenis_kelamin;
      $update->tanggal_lahir = $data->tanggal_lahir;
      $update->nik = $data->nik;
      $update->ktp = $fileName;
      $update->save();

      return redirect('/dashboard');
    }
}
