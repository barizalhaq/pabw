<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
      $quotes = Quote::orderBy('CREATED_AT', 'desc')->take(5)->get();

      return view('pages/landing', compact('quotes'));
    }

    public function showProfile($id = null)
    {
      if($id == null)
        $user = User::findOrFail(Auth::user()->id);
      else
        $user = User::findOrFail($id);
    }
}
