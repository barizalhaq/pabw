<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Quote;

class ContributorController extends Controller
{
    public function dashboard()
    {
      return view('pages/dashboard_contri');
    }

    public function showTulis()
    {
      return view('pages/create_article');
    }

    public function store(Request $request)
    {
      //validasi

      $slug = str_slug($request->title, '-') . '-' . time();
      $fileName = time() . '.png';

      $quotes = Quote::create([
        'title' => $request->title,
        'konten' => $request->konten,
        'slug' => $slug,
        'user_id' => Auth::user()->id,
        'tag' => $request->tag,
        'img' => $fileName,
        $request['img']->storeAs('public/img', $fileName),
      ]);

      return redirect('/dashboard/{user}/konten');
    }

    public function kontenku()
    {
      $quotes = Quote::where('user_id', Auth::user()->id)->orderBy('CREATED_AT', 'desc')->get();

      return view('pages/artikelku', compact('quotes'));
    }

    public function destroyMsg()
    {
      $msg = User::find(Auth::user()->id);
      $msg->alert = 0;
      $msg->save();

      return redirect('/dashboard');
    }

    public function updateDashboard()
    {
      return view('auth/update_contri');
    }

    public function update(Request $data)
    {
      $update = User::find(Auth::user()->id);
      $fileName = time() . '.png';

      $this->validate($data, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users,email,'.$update->id,
        'no_hp' => 'required|numeric|digits_between:8,15',
        'alamat' => 'required|string|min:10',
        'tanggal_lahir' =>'required',
        'foto' => 'mimes:jpg,png,jpeg|max:1000'.$update->id,
      ]);

      $update->name = $data->name;
      $update->email = $data->email;
      if(!empty($data->foto))
      {
        $data['foto']->storeAs('public/profile', $fileName);
        $update->foto = $fileName;
      }
      $update->no_hp = $data->no_hp;
      $update->alamat = $data->alamat;
      $update->save();

      return redirect('/dashboard');
    }

    public function showEdit($slug)
    {
      $quote = Quote::where('slug', $slug)->first();

      return view('pages/edit_article', compact('quote'));
    }

    public function editArtikel(Request $request, $slug)
    {
      $update = Quote::where('slug', $slug)->first();
      $slug = str_slug($request->title, '-') . '-' . time();

      $this->validate($request, [
        'title' => 'required|string|max:255',
        'konten' => 'required',
      ]);

      $update->title = $request->title;
      $update->role = 0;
      $update->slug = $slug;
      $update->konten = $request->konten;
      $update->save();

      return redirect('/dashboard/' . Auth::user()->name . '/konten');
    }

    public function destroy($slug)
    {
      $delete = Quote::where('slug', $slug)->first()->delete();

      return redirect('/dashboard/' . Auth::user()->name . '/konten');
    }

}
