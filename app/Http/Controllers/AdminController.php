<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Quote;
use Illuminate\Http\Request;
use App\Charts\StatistikChart;

class AdminController extends Controller
{
    protected function dashboard()
    {
      return view('pages/admin/admin');
    }

    protected function verif()
    {
      return view('pages/admin/admin_verif');
    }

// Alert == 2 berhasil, alert == 1 gaagal
    protected function statUsers()
    {
      $users = User::all();

      return view('pages/admin/admin_users', compact('users'));
    }

    protected function statKonten()
    {
      $quotes = Quote::all();

      return view('pages/admin/admin_konten', compact('quotes'));
    }

    protected function approveUser($id)
    {
      $user = User::find($id);
      $user->role = 2;
      $user->save();

      return redirect('/dashboard-admin/stat-users');
    }

    protected function suspendUser($id)
    {
      $user = User::find($id);
      $user->role = 1;
      $user->alert = 1;
      $user->save();

      return redirect('/dashboard-admin/stat-users');
    }

// Role 1 terbit, role 2 gagal
    protected function approveKonten($id)
    {
      $quote = Quote::find($id);
      $quote->role = 1;
      $quote->save();

      return redirect('/dashboard-admin/stat-konten');
    }

    protected function suspendKonten($id)
    {
      $quote = Quote::find($id);
      $quote->role = 2;
      $quote->save();

      return redirect('/dashboard-admin/stat-konten');
    }

    public function showUpdate()
    {
      return view('pages/admin/admin_edit');
    }

    public function update(Request $data)
    {
      $user = $msg = User::find(Auth::user()->id);
      $fileName = time() . '.png';

      $this->validate($data, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
        'foto' => 'mimes:jpg,png,jpeg|max:1000'.$user->id,
      ]);

      $user->name = $data->name;
      $user->email = $data->email;
      if(!empty($data->foto))
      {
        $data['foto']->storeAs('public/profile', $fileName);
        $user->foto = $fileName;
      }
      $user->save();

      return redirect('/dashboard-admin');
    }

    public function statistik()
    {
      $users = User::all();
      $today_users = User::whereDate('created_at', today())->count();
      $yesterday_users = User::whereDate('created_at', today()->subDays(1))->count();
      $users_2_days_ago = User::whereDate('created_at', today()->subDays(2))->count();

      $login_hari_ini = User::whereDate('last_login_at', today())->count();
      $login_kemarin = User::whereDate('last_login_at', today()->subDays(1))->count();
      $login_2hari_lalu = User::whereDate('last_login_at', today()->subDays(2))->count();

      $chart = new StatistikChart;
      $chart->labels(['2 Hari lalu', 'Kemarin', 'Hari ini']);
      $chart->dataset('Terdaftar', 'line', [$users_2_days_ago, $yesterday_users, $today_users])->color('#00ccff')->backgroundColor(' #e6faff');
      $chart->dataset('User Login', 'line', [$login_2hari_lalu, $login_kemarin, $login_hari_ini])->color('#ff0066')->backgroundColor('#ffcce0');
      $chart->title('Grafik User');

      $quote_hari_ini = Quote::whereDate('created_at', today())->count();
      $quote_kemarin = Quote::whereDate('created_at', today()->subDays(1))->count();
      $quote_2hari = Quote::whereDate('created_at', today()->subDays(2))->count();
      $total = Quote::all()->count();

      $chart_2 = new StatistikChart;
      $chart_2->labels(['2 Hari lalu', 'Kemarin', 'Hari ini', 'Total']);
      $chart_2->dataset('Total Artikel', 'line', [$quote_2hari, $quote_kemarin, $quote_hari_ini, $total])->color('#00ccff')->backgroundColor(' #e6faff');
      $chart_2->title('Grafik Artikel');

      return view('pages/admin/statistik', compact('chart', 'chart_2', 'users'));
    }
}
