<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Like;
use App\Models\Quote;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function like($type, $model_id)
    {
      if($type == 1)
      {
        $model_type = "App\Models\Quote";
        $model = Quote::find($model_id);
      }


      if($model->isLiked() == 0)
      {
        Like::create([
          'user_id' => Auth::user()->id,
          'likeable_id' => $model_id,
          'likeable_type' => $model_type,
        ]);
      }
      else
        die('0');
    }

    public function unlike($type, $model_id)
    {
      if($type == 1)
      {
        $model_type = "App\Models\Quote";
        $model = Quote::find($model_id);
      }

      if($model->isLiked())
      {
        Like::where('user_id', Auth::user()->id)
                  ->where('likeable_id', $model_id)
                  ->delete();
      }
    }
}
