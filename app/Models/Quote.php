<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{

  protected $fillable = [
      'title', 'slug', 'konten', 'user_id', 'role', 'tag', 'img'
  ];

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function comments()
    {
      return $this->hasMany('App\Models\QuoteComment');
    }

    public function likes()
    {
      return $this->morphMany('App\Models\Like', 'likeable');
    }

    public function isOwner()
    {
      if(Auth::user())
        return Auth::user()->id == $this->user->id;

      return false;
    }

    public function isLiked()
    {
      if(Auth::user())
        return $this->likes->where('user_id', Auth::user()->id)->count();
      else
        return false;
    }
}
