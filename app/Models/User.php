<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'no_hp', 'alamat', 'tanggal_lahir', 'role',
        'nik', 'jenis_kelamin', 'ktp', 'foto', 'last_login_at',
        'last_login_ip',
    ];

    protected $dates = ['dob'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isContributor()
    {
      if($this->role == 2 || $this->role == 0)
        return true;
      else
        return false;
    }

    public function isUser()
    {
      if($this->role == 1)
        return true;
      else
        return false;
    }

    public function isAdmin()
    {
      if($this->role == 3)
        return true;
      else
        return false;
    }

    public function quotes()
    {
      return $this->belongsTo('App\Models\Quote');
    }

    public function comments()
    {
      return $this->hasMany('App\Models\QuoteComment');
    }
}
