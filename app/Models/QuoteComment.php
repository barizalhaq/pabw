<?php
namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class QuoteComment extends Model
{
    protected $table = 'quotecomments';
    protected $fillable = [
      'isi', 'user_id', 'quote_id'
    ];

    public function user()
    {
      return $this->belongsTo('App\Models\User');
    }

    public function quote()
    {
      return $this->belongsTo('App\Models\Quote');
    }

    public function isOwner()
    {
      if(Auth::user())
        return Auth::user()->id == $this->user->id;

      return false;
    }
}
