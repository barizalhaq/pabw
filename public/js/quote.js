$(document).on('click touchstart', '.btn-like', function(){
  var _this = $(this);

  var _url = "/like/" + _this.attr('data-type')
              + "/" + _this.attr('data-model-id');

  $.get(_url, function(data){
    if(data == '0')
    {
      _this.next('.total-like').find('.warning').show().delay(800).fadeOut('slow');
    }
    else
    {
      _this.addClass('btn-danger btn-unlike').removeClass('btn-outline-danger btn-like').html('unlike');
      var likeNumber = _this.parents('.like_wrapper').find('.like-number');
      likeNumber.html(parseInt(likeNumber.html()) + 1);
    }

  });
});

$(document).on('click touchstart', '.btn-unlike', function(){
  var _this = $(this);

  var _url = "/unlike/" + _this.attr('data-type')
              + "/" + _this.attr('data-model-id');

  $.get(_url, function(data){
    _this.removeClass('btn-danger btn-unlike').addClass('btn-outline-danger btn-like').html('like');
    var likeNumber = _this.parents('.like_wrapper').find('.like-number');
    likeNumber.html(parseInt(likeNumber.html()) - 1);
  });

});
