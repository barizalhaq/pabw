<?php
// Home
Route::get('/', 'MainController@index');

// Tampilan Artikel
Route::get('/artikel', 'ArtikelController@index');
Route::get('/artikel/{slug}', 'ArtikelController@show');
Route::get('/artikel/kategori/{kategori}', 'ArtikelController@kategori');
Route::post('/artikel/{slug}/comment', 'CommentController@store');
Route::delete('/artikel/{slug}/comment/delete-{comment}', 'CommentController@destroy');

// Auth
Route::group(['middleware' => 'disablepreventback'],function(){
	Auth::routes();
	Route::get('/home', 'HomeController@index');
});
Route::get('/register-kontributor', 'Auth\ContriRegisterController@showForm');
Route::post('/register-kontributor', 'Auth\ContriRegisterController@register');

// Profile Auth
Route::group(['middleware' => 'user'], function(){
  Route::get('/profile', 'UserController@profile');
  Route::get('/profile/{user}/gabung-kontributor', 'Auth\RegisterController@edit');
	Route::get('/profile/update-profile-{user}', 'UserController@updateProfile');

	Route::put('/profile/update-profile-{user}', 'UserController@update');
  Route::put('/profile/{user}/gabung-kontributor', 'Auth\RegisterController@updateKontributor');
	Route::put('/profile/delete-msg', 'UserController@destroyMsg');
});

// Contributor
Route::group(['middleware' => 'contributor'], function(){
  Route::get('/dashboard', 'ContributorController@dashboard');
  Route::get('/dashboard/tulis-konten', 'ContributorController@showTulis');
  Route::post('/dashboard/tulis-konten', 'ContributorController@store');
  Route::get('/dashboard/{user}/konten', 'ContributorController@kontenku');
	Route::get('/dashboard/update-profile-{user}', 'ContributorController@updateDashboard');
	Route::get('/dashboard/{slug}/edit', 'ContributorController@showEdit');

	Route::delete('/dashboard/{slug}/delete', 'ContributorController@destroy');
	Route::put('/dashboard/update-profile-{user}', 'ContributorController@update');
	Route::put('/dashboard/{slug}/edit', 'ContributorController@editArtikel');
	Route::put('/dashboard/{slug}/delete', 'ContributorController@deleteArtikel');
	Route::put('/dashboard/delete-msg', 'ContributorController@destroyMsg');
});

Route::group(['middleware' => 'auth'], function(){
  Route::get('/home', function(){
    return redirect('/');
  });
	Route::get('/like/{type}/{model}', 'LikeController@like');
	Route::get('/unlike/{type}/{model}', 'LikeController@unlike');
});

Route::group(['middleware' => 'admin'], function(){
  Route::get('/dashboard-admin', 'AdminController@dashboard');
	Route::get('/dashboard-admin/update-profile-{user}', 'AdminController@showUpdate');
  Route::get('/dashboard-admin/verifikasi', 'AdminController@verif');
  Route::get('/dashboard-admin/stat-users', 'AdminController@statUsers');
  Route::get('/dashboard-admin/stat-konten', 'AdminController@statKonten');
	Route::get('/dashboard-admin/statistik', 'AdminController@statistik');

  Route::put('/dashboard-admin/stat-users/{user}/approve', 'AdminController@approveUser');
  Route::put('/dashboard-admin/stat-users/{user}/suspend', 'AdminController@suspendUser');
  Route::put('/dashboard-admin/stat-konten/{konten}/approve', 'AdminController@approveKonten');
  Route::put('/dashboard-admin/stat-konten/{konten}/suspend', 'AdminController@suspendKonten');
	Route::put('/dashboard-admin/update-profile-{admin}', 'AdminController@update');
});

// Route::get('/profile/{id?}', 'MainController@showProfile');
