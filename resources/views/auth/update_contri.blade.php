@extends('layouts.profilling')

@section('title')

@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
  <section>

    <div class="container">
      <div class="col-sm-12 card shadow ">
        <div class="row">
         <div class="col-sm-12">
           <a href="/dashboard" style="color: #EE7064">{{ __("Kembali ke dashboard") }}</a>
            <br>
           <h3 class="text-center">Edit Profile</h3>
            <hr>
            <br>
         </div>
       </div>

       <form method="post" action="/dashboard/update-profile-{{Auth::user()->name}}" enctype="multipart/form-data">
       @csrf

       <div class="form-group row">
           <label for="file" class="col-md-4 col-form-label text-md-right">{{ __('Foto Profile') }}</label>
           <div class="col-md-6">
               <input id="file" type="file" name="foto" value="{{Auth::user()->foto}}">
               <button type="button" class="close" aria-label="Close" onclick="document.getElementById('file').value = ''">
                 <span aria-hidden="true">&times;</span>
               </button>
               <input type="hidden" name="" value="" class="form-control{{ $errors->has('foto') ? ' is-invalid' : '' }}">
               @if ($errors->has('foto'))
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('foto') }}</strong>
                   </span>
               @endif
           </div>
       </div>

       <div class="form-group row">
           <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

           <div class="col-md-6">
               <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{Auth::user()->name}}" required autofocus>

               @if ($errors->has('name'))
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('name') }}</strong>
                   </span>
               @endif
           </div>
       </div>

       <div class="form-group row">
           <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

           <div class="col-md-6">
               <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{Auth::user()->email}}" required>

               @if ($errors->has('email'))
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('email') }}</strong>
                   </span>
               @endif
           </div>
       </div>

       <div class="form-group row">
           <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

           <div class="col-md-6">
               <textarea id="alamat" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat" required rows="4" cols="10" autofocus>{{Auth::user()->alamat}}</textarea>
               @if ($errors->has('alamat'))
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('alamat') }}</strong>
                   </span>
               @endif
           </div>
       </div>

       <div class="form-group row">
           <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Hp') }}</label>

           <div class="col-md-6">
               <input id="no_hp" type="text" class="form-control{{ $errors->has('no_hp') ? ' is-invalid' : '' }}" name="no_hp" value="{{Auth::user()->no_hp}}" required autofocus>

               @if ($errors->has('no_hp'))
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('no_hp') }}</strong>
                   </span>
               @endif
           </div>
       </div>

       <div class="form-group row">
           <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

           <div class="col-md-6">
               <input id="tanggal_lahir" type="date" class="form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}" name="tanggal_lahir" value="{{Auth::user()->tanggal_lahir}}" required autofocus>

               @if ($errors->has('tanggal_lahir'))
                   <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                   </span>
               @endif
           </div>
       </div>

       <div class="form-group row">
         <div class="col-sm-12 text-right">
           <div class="col-md-6 offset-md-4">
               <button type="submit" class="btn btn-primary">
                   {{ __('Simpan') }}
               </button>
               <input type="hidden" name="_method" value="put">
               <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
           </div>
         </div>
       </div>
   </form>


      </div>
    </div>

  </section>
</div>
@endsection
