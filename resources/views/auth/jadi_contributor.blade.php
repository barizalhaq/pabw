@extends('layouts.profilling')

@section('title')
  Halo {{Auth::user()->name}}
@endsection

@section('content')
  <div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">

    <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
          <div class="col">
            @if(Auth::user()->foto==null)
              @if(Auth::user()->jenis_kelamin=='laki')
                <a class="nav-link" href="/profile"><img src="{{asset('pic/cowo.png')}}" style="width: 50%"></a><br>
                @else
                <a class="nav-link" href="/profile"><img src="{{asset('pic/cewe.png')}}" style="width: 50%"></a><br>
              @endif
            @else
              <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
            @endif
              <h5>Profile</h5>
            </div>
          <div class="col">
            <a class="nav-link" href="/dashboard/{{Auth::user()->name}}/konten"><img src="{{asset('pic/konten.png')}}" style="width: 50%"></a><br>
            <h5>Konten</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard/tulis-konten"><img src="{{asset('pic/kebutuhan.png')}}" style="width: 50%"></a><br>
            <h5>Tulis Konten</h5>
          </div>
        </div>
        </div>
      </center>
    </div>

    <div class="container">
      <div class="col-sm-12 card shadow ">
        <div class="row">
         <div class="col-sm-12">
           <br>
           <h3 class="text-center">Gabung Kontributor</h3>
            <hr>
            <br>
         </div>
       </div>
        <form method="post" action="/profile/{{Auth::user()->name}}/gabung-kontributor" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{Auth::user()->name}}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{Auth::user()->email}}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                <div class="col-md-6">
                    <textarea id="alamat" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat" required rows="4" cols="10" autofocus>{{ old('alamat') }}</textarea>
                    @if ($errors->has('alamat'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="no_hp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Hp') }}</label>

                <div class="col-md-6">
                    <input id="no_hp" type="text" class="form-control{{ $errors->has('no_hp') ? ' is-invalid' : '' }}" name="no_hp" value="{{ old('no_hp') }}" required autofocus>

                    @if ($errors->has('no_hp'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('no_hp') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="nik" class="col-md-4 col-form-label text-md-right">{{ __('NIK') }}</label>

                <div class="col-md-6">
                    <input id="nik" type="text" class="form-control{{ $errors->has('nik') ? ' is-invalid' : '' }}" name="nik" value="{{ old('nik') }}" required autofocus>

                    @if ($errors->has('nik'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nik') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="tanggal_lahir" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>

                <div class="col-md-6">
                    <input id="tanggal_lahir" type="date" class="form-control{{ $errors->has('tanggal_lahir') ? ' is-invalid' : '' }}" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required autofocus>

                    @if ($errors->has('tanggal_lahir'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tanggal_lahir') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="jenis_kelamin" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Kelamin') }}</label>

                <div class="col-md-6">

                  <select name="jenis_kelamin" class="form-control{{ $errors->has('jenis_kelamin') ? ' is-invalid' : '' }}">
                    <option value="laki">Laki - Laki</option>
                    <option value="perempuan">Perempuan</option>
                  </select>

                    @if ($errors->has('jenis_kelamin'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('jenis_kelamin') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="uploadKTP" class="col-md-4 col-form-label text-md-right">{{ __('Upload KTP') }}</label>

                <div class="col-md-6">
                    <input id="uploadKTP" type="file" class="" name="ktp" required>
                    <input type="hidden" name="" value="" class="form-control{{ $errors->has('ktp') ? ' is-invalid' : '' }}">
                    @if ($errors->has('ktp'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('ktp') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-12 text-right">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Simpan') }}
                    </button>
                    <input type="hidden" name="_method" value="put">
                    <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
    </div>

  </div>
@endsection
