@extends('layouts.master')

@section('title')
  Artikel ZIAGA
@endsection

@section('header')
  <div class="jumbotron" style="background-color: #ff6666; margin-top: -40px; min-height: 50%; padding-left: 90px;
  padding-right: 90px;">
    <img src="{{asset('pic/787.jpg')}}" class="shake-vertical rounded float-right d-inline-flex p-2" width="25%" style="margin-top: 20px;">
    <h1 class="display-4">Artikel Edukasi Bencana</h1>
    <p class="lead">Memberikan edukasi dan tips kepada masyarakat umum saat terjadinya bencana maupun pasca bencana.</p>
    <p>Ketahuilah tanda tandanya. Selamatkan diri anda dan keluarga.</p>
  </div>
@endsection

@section('content')
<div class="container">
  <section>
    <div class="form-group row w-50 p-3 float-left">
      <form class="form-inline my-2 my-lg-0" action="/artikel" method="get">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
        <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>

    <div class="form-group row w-50 p-3 float-right">
        <label for="tag" class="col-sm-2 col-form-label">Kategori</label>
        <div class="col-sm-10">
          <select name="tag" class="form-control" id="tag" onchange="location = this.value;">
            <option value="" disabled selected>Pilih Tag</option>
            <option value="{{url('/artikel/kategori/gempa-bumi')}}">
              Gempa Bumi
            </option>
            <option value="{{url('/artikel/kategori/banjir')}}">
              Banjir
            </option>
            <option value="{{url('/artikel/kategori/gunung-meletus')}}">
              Gunung Meletus
            </option>
            <option value="{{url('/artikel/kategori/kebakaran')}}">
              Kebakaran
            </option>
            <option value="{{url('/artikel/kategori/tanah-longsor')}}">
              Tanah Longsor
            </option>
            <option value="{{url('/artikel/kategori/badai')}}">
              Badai
            </option>
            <option value="{{url('/artikel/kategori/puting-beliung')}}">
              Puting Beliung
            </option>
            <option value="{{url('/artikel/kategori/tsunami')}}">
              Tsunami
            </option>
          </select>
        </div>
    </div>
  </section>

  @if($quotes->isEmpty())
  <div class="alert alert-warning" role="alert" style="font-weight:bold">
    <center>
        Artikel tidak ditemukan.
    </center>
  </div>
  @endif

</div>

@foreach($quotes as $quote)
  <section class="profil " id="profil" style="margin-top: auto">
    <div class="container">

      <div class="col-sm-12 card hover">
        <a class=" list-group-item-action" href="/artikel/{{$quote->slug}}">
        <div class="row">
            <div class="card-body">
              <h5 class="card-title">{{$quote->title}}</h5>
            </div>
          <div class="col-sm-1 ">
            <div  style="height: 100%; width: 0px; ">
            </div>
          </div>
          <div class="col-sm-3">
            <div class="card-body">
              <table style="margin: -10px; width: 100%">
                    <tr>
                      <td style="padding-left: 30px; width: 50%;">
                      <a class="btn btn-danger" style="background-color: #EE7064;" data-toggle="collapse" href="#collapseExample{{$quote->slug}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$quote->slug}}">Sekilas</a>
                      </td>
                    </tr>
              </table>
            </div>
          </div>
        </div>
        </a>
      </div>

      <div class="collapse" id="collapseExample{{$quote->slug}}">
        <div class="card card-body">
          {!! Str::words($quote->konten, 10,'....') !!}
        </div>
      </div>
    </div>
  </section>
  @endforeach

  <section>
    <div class="container pagination justify-content-center">
        {!!$quotes->render()!!}
    </div>
  </section>

@endsection
