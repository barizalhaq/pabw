@extends('layouts.master')

@section('title')
  ZIAGA - {{$quote->title}}
@endsection

@section('content')
  <div style="color: #425365; background-color: #ffffff; margin-top: 5%;">
    <img src="{{asset('storage/img/' . $quote->img)}}" alt="Responsive image" class="img-fluid mx-auto d-block" width="950">
    <br>
  </div>

    <section class="profil " id="profil">
      <div class="container">

        <section class="profil " id="profil">
          <div class="container">
            <h1 class="text-left font-weight-bold">{{$quote->title}}</h1>
            <div class="text-justify">
              {!!$quote->konten!!}
            </div>


              <div style="margin-bottom: 3%">
                <hr>
                <div class="row">
                  <div class="col-sm-8">
                    <h5>Bagikan :</h5>
                    <table style="margin-top: 40px">
                      <tr>
                        <td style="padding-left: 10px"><a href="https://www.whatsapp.com"><img src="{{asset('pic/wa.png')}}" style="width: 40px"></a></td>
                        <td style="padding-left: 10px"><a href="https://line.me/en/"><img src="{{asset('pic/line.png')}}" style="width: 40px"></a></td>
                        <td style="padding-left: 10px"><a href="https://facebook.com"><img src="{{asset('pic/fb.png')}}" style="width: 40px"></a></td>
                        <td style="padding-left: 10px"><a href="https://twitter.com"><img src="{{asset('pic/tw.png')}}" style="width: 40px"></a></td>
                      </tr>
                    </table>
                  </div>

                  <div class="col-sm-3">
                    <center>

                      <div class="like_wrapper">
                        <div class="btn {{$quote->isLiked() ? 'btn-danger btn-unlike' : 'btn-outline-danger btn-like'}}"
                        data-model-id="{{$quote->id}}" data-type="1">
                          {{$quote->isLiked() ? 'unlike' : 'like'}}
                        </div>
                        <div class="total-like">
                          <span class="like-number"> {{$quote->likes->count()}} </span> Like
                          <span class="warning" style="display: none;">Harus login terlebih dahulu!</span>
                        </div>
                      </div>
                        <br>
                      @if($quote->isOwner())
                      <table>
                        <tr>
                          <td>
                            <a href="/dashboard/{{$quote->slug}}/edit" class="btn btn-primary btn-sm">
                            {{__('Edit')}}
                          </a>
                        </td>
                          <td>
                            <form action="/dashboard/{{$quote->slug}}/delete" method="post">
                              <button class="btn btn-danger btn-sm" type="submit" name="button" onclick="return confirm('Anda yakin ingin menghapus?');">
                                {{__('Hapus')}}
                              </button>
                              <input type="hidden" name="_method" value="delete">
                              {{csrf_field()}}
                            </form>
                          </td>
                        </tr>
                      </table>
                      @endif
                    </center>
                  </div>

                </div>
              </div>

              <div class="card text-center" style="margin-bottom: 5%">
                <div class="card-header font-weight-bold">
                  Penulis
                </div>
                <div class="card-body">
                  @if($quote->user->foto == null && $quote->user->jenis_kelamin=='laki')
                    <p class="card-text"><img src="{{asset('pic/cowo.png')}}" class="img-circle" alt="Gambar Profile" height="100"> </p>
                  @elseif($quote->user->jenis_kelamin=='perempuan')
                    <p class="card-text"><img src="{{asset('pic/cewe.png')}}" class="img-circle" alt="Gambar Profile" height="100"> </p>
                  @else
                    <p class="card-text"><img src="{{asset('storage/profile/' . $quote->user->foto)}}" class="img-circle" alt="Gambar Profile" height="100"> </p>
                  @endif
                  <center>
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th>Nama</th>
                          <th>:</th>
                          <td>{{ ucfirst(trans($quote->user->name)) }}</td>
                        </tr>
                        <tr>
                          <th>Jenis Kelamin</th>
                          <th>:</th>
                          @if($quote->user->jenis_kelamin == 'laki')
                            <td>{{ ucfirst(trans('Laki - Laki')) }}</td>
                          @else
                            <td>{{ ucfirst(trans('Perempuan')) }}</td>
                          @endif
                        </tr>
                      </table>
                    </div>
                </center>
                </div>
                <div class="card-footer text-muted">
                  {{Carbon\Carbon::parse($quote->created_at)->diffForHumans()}}
                </div>
              </div>

              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item font-weight-bold">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Komentar</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"></div>
              </div>
              <br>

              @if($quote->comments->isEmpty())
              <div class="alert alert-warning" role="alert" style="font-weight:bold">
                <center>
                    Belum ada komentar.
                </center>
              </div>
              @endif

              @foreach($quote->comments as $comment)
                <div class="card mb-3" style="max-width: 18rem; border-color: #bfbfbf !important">
                  <div class="card-header">{{$comment->user->name}}</div>
                  <div class="card-body text-secondary">
                    <h5 class="card-title">{{$comment->isi}}</h5>
                    <p class="font-italic">{{$comment->created_at}}</p>
                    <p class="font-italic">{{$comment->created_at->diffForHumans()}}</p>
                    @if($comment->isOwner())
                      <form action="/artikel/{{$quote->slug}}/comment/delete-{{$comment->id}}" method="post">
                        <button type="submit" class="badge badge-danger" onclick="return confirm('Anda yakin ingin menghapus komentar?');">Hapus</button>
                        <input type="hidden" name="_method" value="delete">
                        {{csrf_field()}}
                      </form>
                    @endif
                  </div>
                </div>
              @endforeach

            <hr>


            <h5>Tulis Komentar</h5>

            @if($quote->role == 0)
            <section>
              <div class="alert alert-warning" role="alert" style="font-weight:bold">
                <center>
                    Fitur belum bisa digunakan, mununggu persetujuan admin.
                </center>
              </div>
            </section>
            @else
            <section>
              <form action="/artikel/{{$quote->slug}}/comment" method="post" style="margin-bottom: 50px;">
                <div class="form-group">
                  <textarea name="komentar" class="form-control{{ $errors->has('komentar') ? ' is-invalid' : '' }}" rows="8" cols="80"></textarea>
                  @if ($errors->has('komentar'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('komentar') }}</strong>
                      </span>
                  @endif
                    {{csrf_field()}}
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                  {{__('Submit')}}
                </button>
              </form>
            </section>
            @endif

          </div>
        </section>
      </div>
    </section>

@endsection
