@extends('layouts.profilling')

@section('title')
  Halo {{Auth::user()->name}}
@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
  <section>

    <div class="jumbotron jumbotron-fluid" style="background-color: #fff; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">

          <div class="row">
            <div class="col">
              <a class="nav-link"><hr style="border-color: #425365"></a>
            </div>
            <div class="col">
              <a class="nav-link"><h3 style="color: #425365">Selamat Datang {{Auth::user()->name}}</h3></a>
            </div>
            <div class="col">
              <a class="nav-link"><hr style="border-color: #425365"></a>
            </div>
        </div>

      </div>
      </center>
    </div>

    <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">

      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
          <div class="col">
          @if(Auth::user()->foto==null)
            @if(Auth::user()->jenis_kelamin=='laki')
              <a class="nav-link" href="/profile"><img src="{{asset('pic/cowo.png')}}" style="width: 50%"></a><br>
              @else
              <a class="nav-link" href="/profile"><img src="{{asset('pic/cewe.png')}}" style="width: 50%"></a><br>
            @endif
          @else
            <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
          @endif
            <h5>Profile</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard/{{Auth::user()->name}}/konten"><img src="{{asset('pic/konten.png')}}" style="width: 50%"></a><br>
            <h5>Konten</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard/tulis-konten"><img src="{{asset('pic/kebutuhan.png')}}" style="width: 50%"></a><br>
            <h5>Tulis Konten</h5>
          </div>
        </div>
        </div>
      </center>
    </div>

    @if(Auth::user()->alert==1)
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Anda telah gagal menjadi kontributor!</strong> Data anda tidak valid.
        <form action="/profile/delete-msg" method="post">
          <button type="submit" class="close">
            <span aria-hidden="true">&times;</span>
          </button>
          {{csrf_field()}}
          <input type="hidden" name="_method" value="put">
        </form>
      </div>
    @endif

    <div class="container">
      <div class="col-sm-12 card shadow ">
       <div class="row">
        <div class="col-sm-12">
          <br>
          <h2 class="text-center">Profile</h2>
          <hr>
          @if(Auth::user()->foto==null)
           @if(Auth::user()->jenis_kelamin=='laki')
             <h2 class="text-center"><img src="{{asset('pic/cowo.png')}}" class="rounded-circle" width="200px"></h2>
             @else
             <h2 class="text-center"><img src="{{asset('pic/cewe.png')}}" class="rounded-circle" width="200px"></h2>
           @endif
          @else
            <h2 class="text-center"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" width="200px"></h2>
          @endif
        </div>
      </div>
      <form>
        <div class="form-group row">
          <label for="validationDefault01" class="col-sm-2 col-form-label">Nama</label>
          <div class="col-sm-10">
            <input readonly class="form-control" id="validationDefault01" value="{{Auth::user()->name}}" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="validationDefault01" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
            <input readonly class="form-control" id="validationDefault01" value="{{Auth::user()->email}}" required>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12 text-right">
            <a href="{{ url('/profile/update-profile-' . Auth::user()->name) }}" type="button" class="btn btn-primary">
              Edit
            </a>
            <br>
            <br>
            <a href="/profile/{{Auth::user()->name}}/gabung-kontributor" style="color: #EE7064">Jadi Kontributor?</a>
          </div>
        </div>
        <div class="alert alert-secondary" role="alert">
          Anda harus bergabung menjadi contributor dahulu untuk fitur tulis konten.
        </div>
      </form>

    </div>

    </div>

  </section>
</div>
@endsection
