@extends('layouts.profilling')

@section('title')
  Buat Article
@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
  <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
    <center>
      <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
        <div class="row">
        <div class="col">
          @if(Auth::user()->foto==null)
            @if(Auth::user()->jenis_kelamin=='laki')
              <a class="nav-link" href="/dashboard"><img src="{{asset('pic/cowo.png')}}" class="rounded-circle" style="width: 50%"></a><br>
              @else
              <a class="nav-link" href="/dashboard"><img src="{{asset('pic/cewe.png')}}" class="rounded-circle" style="width: 50%"></a><br>
            @endif
          @else
            <a class="nav-link" href="/dashboard"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
          @endif
          <h5>Profile</h5>
        </div>
        <div class="col">
          <a class="nav-link" href="/dashboard/{{Auth::user()->name}}/konten"><img src="{{asset('pic/konten.png')}}" style="width: 50%"></a><br>
          <h5>Konten</h5>
        </div>
        <div class="col">
          <a class="nav-link" href="/dashboard/tulis-konten"><img src="{{asset('pic/kebutuhan.png')}}" style="width: 50%"></a><br>
          <h5>Tulis Konten</h5>
        </div>
      </div>
      </div>
    </center>
  </div>


    <div class="container">
      <center>

        <div class="col-sm-12 card shadow ">
         <div class="row">
          <div class="col-sm-12">
            <h2 class="text-center">Tulis Konten</h2> <hr>
          </div>
        </div>

        @if(Auth::user()->role==0)
          <div class="alert alert-warning" role="alert" style="font-weight:bold">
            <center>
              Sedang menunggu persetujuan admin.
          </center>
          </div>
        @else
          <form action="/dashboard/tulis-konten" method="post" enctype="multipart/form-data">

            <div class="form-group row">
              <label for="title" class="col-sm-2 col-form-label">Judul</label>
              <div class="col-sm-10">
                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" name="title" value="{{old('title')}}" required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
                <label for="img" class="col-sm-2 col-form-label">{{ __('Gambar') }}</label>
                <div class="col-sm-4">
                    <input id="img" type="file" class="" name="img" required>
                    <input type="hidden" name="" value="" class="form-control{{ $errors->has('img') ? ' is-invalid' : '' }}">

                    @if ($errors->has('img'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('img') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="tag" class="col-sm-2 col-form-label">Tag</label>
                <div class="col-sm-10">
                  <select name="tag" class="form-control{{ $errors->has('tag') ? ' is-invalid' : '' }}" id="tag">
                    <option value="" disabled selected>Pilih Tag</option>
                    <option value="gempa-bumi">Gempa Bumi</option>
                    <option value="banjir">Banjir</option>
                    <option value="gunung-meletus">Gunung Meletus</option>
                    <option value="kebakaran">Kebakaran</option>
                    <option value="tanah-longsor">Tanah Longsor</option>
                    <option value="badai">Badai</option>
                    <option value="puting-beliung">Puting Beliung</option>
                    <option value="tsunami">Tsunami</option>
                  </select>

                    @if ($errors->has('tag'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tag') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
              <label for="editor" class="col-sm-2 col-form-label">Konten</label>
              <div class="col-sm-10">
                <textarea name="konten" id="editor" class="wysiwyg form-control{{ $errors->has('konten') ? ' is-invalid' : '' }}"></textarea>

                @if ($errors->has('konten'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('konten') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-12 text-right">
                <button type="submit" class="btn btn-primary">
                  Submit
                </button>
              </div>
            </div>
          </div>
          {{csrf_field()}}
          </form>
        @endif
        </center>
      </div>
    </div>

    <script>
      CKEDITOR.replace("editor")
    </script>

@endsection
