@extends('layouts.master')

@section('title')
  ZIAGA
@endsection

@section('header')
  <div class="jumbotron jumbotron-fluid text-center " style="background: url({{asset('pic/home.png')}}) center center" >
  </div>
@endsection

@section('content')
  <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
    <center>
      <div class="container" >
        <div class="row">
          <div class="col">
          </div>
          <div class="col">
              <img src="{{asset('pic/homeicon.png')}}" style="width: 40px; margin-top: -100px;"><br>
              <p style="margin-top: -40px">Home</p>
          </div>
          <div class="col">
          </div>
      </div>
      </div>
    </center>

    <div class="container" >
      <h1 class="display-4" style="padding-top: 10px; width:100%">Ziaga</h1>
      <p class="lead text-justify">
        Portal Edukasi Kesiapsiagaan bencana adalah sebuah perangkat lunak berbasis web yang
        berfungsi untuk memberikan masyarakat pengetahuan dalam menanggulani kejadian pra
        bencana dan pasca bencana. Selain itu, perangkat lunak ini juga berfungsi sebagai wadah bagi
        pihak-pihak maupun sekelompok orang yang memiliki pengetahuan lebih dan sudah
        diverifikasi oleh BNPB berbagi pengetahuan yang dimilikinya
      </p>
    </div>
  </div>

  <div class="jumbotron jumbotron-fluid" style="color: #425365; background-color: #ffffff; margin-top: 50px; min-height: 50% ; margin-bottom: 50px">
    <center>
      <div class="container" >
        <div class="row">
        <div class="col">
        </div>
        <div id="kat" class="col">
          <img src="{{asset('pic/kategori.png')}}" style="width: 50px; margin-top: -100px;"><br>
          <p style="margin-top: -40px;  color: #425365">Kategori</p>
        </div>
        <div class="col">
        </div>
      </div>

      </div>
    </center>



    <center>
      <div class="container" style="margin-top: 50px">
        <div class="row">
        <div class="col">
          <a href="/artikel/kategori/gempa-bumi"><img src="{{asset('pic/gempa.png')}}" style="width: 100px"><br>
          <p style=" color: #425365">Gempa Bumi</p></a>
        </div>
        <div class="col">
          <a href="/artikel/kategori/banjir"><img src="{{asset('pic/banjir.png')}}" style="width: 100px"><br>
          <p style=" color: #425365">Banjir</p></a>
        </div>
        <div class="col">
          <a href="/artikel/kategori/kebakaran"><img src="{{asset('pic/kebakaran.png')}}" style="width: 100px"><br>
          <p style=" color: #425365">Kebakaran</p></a>
        </div>
        <div class="col">
          <a href="/artikel/kategori/tanah-longsor"><img src="{{asset('pic/tanahlongsor.png')}}" style="width: 100px"><br>
          <p style=" color: #425365">Tanah Longsor</p></a>
        </div>
      </div>

        <div class="row">
          <div class="col">
            <a href="/artikel/kategori/gunung-meletus"><img src="{{asset('pic/gungmeletus.png')}}" style="width: 100px"><br>
            <p style=" color: #425365">Gunung Meletus</p></a>
          </div>
          <div class="col">
            <a href="/artikel/kategori/tsunami"><img src="{{asset('pic/tsunami.png')}}" style="width: 100px"><br>
            <p style=" color: #425365">Tsunami</p></a>
          </div>
          <div class="col">
            <a href="/artikel/kategori/badai"><img src="{{asset('pic/badai.png')}}" style="width: 100px"><br>
            <p style=" color: #425365">Badai</p></a>
          </div>
          <div class="col">
            <a href="/artikel/kategori/puting-beliung"><img src="{{asset('pic/putingbeliung.png')}}" style="width: 100px"><br>
            <p style=" color: #425365">Puting Beliung</p></a>
          </div>
        </div>
      </div>
    </center>
  </div>

  <div class="jumbotron jumbotron-fluid" style="color: #425365; background-color: #ffffff; margin-top: -40px; min-height: 50%">
    <center>
      <div class="container" >
        <div class="row">
        <div class="col">
        </div>
        <div id="kat" class="col">
          <img src="{{asset('pic/konten.jpg')}}" style="width: 50px; margin-top: -100px;"><br>
          <p style="margin-top: -30px;  color: #425365">Artikel Terbaru</p>
        </div>
        <div class="col">
        </div>
      </div>
      </div>
    </center>

      <div class="container" style="margin-top: 50px">
        @foreach($quotes as $quote)
          @if($quote->role==1)
          <section class="profil " id="profil" style="margin-top: auto">
            <div class="container">

              <div class="col-sm-12 card hover">
                <a class=" list-group-item-action" href="/artikel/{{$quote->slug}}">
                <div class="row">
                    <div class="card-body">
                      <h5 class="card-title">{{$quote->title}}</h5>
                    </div>
                  <div class="col-sm-1 ">
                    <div  style="height: 100%; width: 0px; ">
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="card-body">
                      <table style="margin: -10px; width: 100%">
                            <tr>
                              <td style="padding-left: 30px; width: 50%;">
                              <a class="btn btn-danger" style="background-color: #EE7064;" data-toggle="collapse" href="#collapseExample{{$quote->slug}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$quote->slug}}">Sekilas</a>
                              </td>
                            </tr>
                      </table>
                    </div>
                  </div>
                </div>
                </a>
              </div>

              <div class="collapse" id="collapseExample{{$quote->slug}}">
                <div class="card card-body">
                  {!! Str::words($quote->konten, 10,'....') !!}
                </div>
              </div>
            </div>
          </section>
            @endif
          @endforeach
      </div>
  </div>
@endsection
