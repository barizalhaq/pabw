@extends('layouts.profilling')

@section('title')
  Konten {{Auth::user()->name}}
@endsection

@section('content')
    <div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">

      <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
        <center>
          <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
            <div class="row">
            <div class="col">
              @if(Auth::user()->foto==null)
                @if(Auth::user()->jenis_kelamin=='laki')
                  <a class="nav-link" href="/dashboard"><img src="{{asset('pic/cowo.png')}}" class="rounded-circle" style="width: 50%"></a><br>
                  @else
                  <a class="nav-link" href="/dashboard"><img src="{{asset('pic/cewe.png')}}" class="rounded-circle" style="width: 50%"></a><br>
                @endif
              @else
                <a class="nav-link" href="/dashboard"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
              @endif
              <h5>Profile</h5>
            </div>
            <div class="col">
              <a class="nav-link" href="/dashboard/{{Auth::user()->name}}/konten"><img src="{{asset('pic/konten.png')}}" style="width: 50%"></a><br>
              <h5>Konten</h5>
            </div>
            <div class="col">
              <a class="nav-link" href="/dashboard/tulis-konten"><img src="{{asset('pic/kebutuhan.png')}}" style="width: 50%"></a><br>
              <h5>Tulis Konten</h5>
            </div>
          </div>
          </div>
        </center>
      </div>

      @if($quotes->isEmpty())
      <div class="alert alert-warning" role="alert" style="font-weight:bold">
        <center>
            Anda tidak mempunyai konten apapun.
        </center>
      </div>
      @endif

        @foreach($quotes as $quote)
        <section class="profil " id="profil" style="margin: auto">
          <div class="container">

            <div class="col-sm-12 card hover">
              <a class=" list-group-item-action" href="/artikel/{{$quote->slug}}">
              <div class="row">
                <!-- <div class="col-sm-8"> -->
                  <div class="card-body">
                    <h5 class="card-title">{{$quote->title}}</h5>

                    @if($quote->role == 0)
                        <p class="card-text alert-warning alert" style="width:300px; font-size:12px; font-weight:bold">Menunggu persetujuan admin untuk terbit.</p>

                    @elseif($quote->role == 2)
                        <p class="card-text alert-danger alert" style="width:300px; font-size:12px; font-weight:bold">Gagal terbit.</p>
                    @endif
                  </div>
                <!-- </div> -->
                <div class="col-sm-1 ">
                  <div  style="height: 100%; width: 0px; ">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="card-body">
                    <table style="margin: -10px; width: 100%">
                          <tr>
                            <td style="padding-left: 30px; width: 50%;">
                            <a class="btn btn-danger" style="background-color: #EE7064;" data-toggle="collapse" href="#collapseExample{{$quote->slug}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$quote->slug}}">Sekilas</a>
                            </td>
                          </tr>
                    </table>
                  </div>
                </div>
              </div>
              </a>
            </div>

            <div class="collapse" id="collapseExample{{$quote->slug}}">
              <div class="card card-body">
                <!-- <p>{{ strip_tags(str_limit($quote->konten, 15)) }}</p> -->
                {!! Str::words($quote->konten, 10,'....') !!}
              </div>
            </div>
          </div>
        </section>
      @endforeach

  </div>

@endsection
