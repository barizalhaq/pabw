@extends('layouts.profilling')

@section('title')
  Edit Artikel
@endsection

@section('content')
  <div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
    <div class="container">
      <center>

        <div class="col-sm-12 card shadow ">
         <div class="row">
          <div class="col-sm-12">
            <h2 class="text-center">Edit Konten</h2> <hr>
          </div>
        </div>

        @if(Auth::user()->role==0)
          <div class="alert alert-warning" role="alert" style="font-weight:bold">
            <center>
              Sedang menunggu persetujuan admin.
          </center>
          </div>
        @else
          <form action="/dashboard/{{$quote->slug}}/edit" method="post">

            <div class="form-group row">
              <label for="title" class="col-sm-2 col-form-label">Judul</label>
              <div class="col-sm-10">
                <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" name="title"
                value="{{old('title') ? old('title') : $quote->title}}" required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
                <label for="tag" class="col-sm-2 col-form-label">Tag</label>
                <div class="col-sm-10">
                  <select name="tag" class="form-control{{ $errors->has('tag') ? ' is-invalid' : '' }}" id="tag">
                    <option value="" disabled selected>Pilih Tag</option>
                    <option value="Gempa Bumi">Gempa Bumi</option>
                    <option value="Banjir">Banjir</option>
                    <option value="Gunung Meletus">Gunung Meletus</option>
                    <option value="Kebakaran">Kebakaran</option>
                    <option value="Tanah Longsor">Tanah Longsor</option>
                    <option value="Badai">Badai</option>
                    <option value="Puting Beliung">Puting Beliung</option>
                    <option value="Tsunami">Tsunami</option>
                  </select>

                    @if ($errors->has('tag'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tag') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
              <label for="title" class="col-sm-2 col-form-label">Konten Lama</label>
              <div class="col-sm-10">
                {!!$quote->konten!!}
              </div>
            </div>

            <div class="form-group row">
              <label for="editor" class="col-sm-2 col-form-label">Konten</label>
              <div class="col-sm-10">
                <textarea name="konten" id="editor" class="wysiwyg form-control{{ $errors->has('konten') ? ' is-invalid' : '' }}"></textarea>

                @if ($errors->has('konten'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('konten') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-12 text-right">
                <button type="submit" class="btn btn-primary">
                  Submit
                </button>
                <input type="hidden" name="_method" value="put">
                <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
              </div>
            </div>
          </div>
          {{csrf_field()}}
          </form>
        @endif
        </center>
      </div>
  </div>

  <script>
    CKEDITOR.replace("editor")
  </script>
@endsection
