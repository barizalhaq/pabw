@extends('layouts.profilling')

@section('title')
  Selamat Datang Admin
@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
  <section>

    <div class="jumbotron jumbotron-fluid" style="background-color: #fff; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
            <div class="col">
              <a class="nav-link"><hr style="border-color: #425365"></a>
            </div>
            <div class="col">
              <a class="nav-link"><h3 style="color: #425365">Selamat Datang Admin</h3></a>
            </div>
            <div class="col">
              <a class="nav-link"><hr style="border-color: #425365"></a>
            </div>
          </div>
        </div>
      </center>
    </div>

    <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
          <div class="col">
            @if(Auth::user()->foto==null)
              <a class="nav-link" href="/dashboard-admin"><img src="{{asset('pic/admin.png')}}" style="width: 50%"></a><br>
            @else
              <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
            @endif
            <h5>Profile</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/verifikasi"><img src="{{asset('pic/verif.png')}}" style="width: 50%"></a><br>
            <h5>Verifikasi</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/statistik"><img src="{{asset('pic/statistik.png')}}" style="width: 50%"></a><br>
            <h5>Statistik</h5>
          </div>
        </div>
        </div>
      </center>
    </div>

    <div class="container">
      <div class="col-sm-12 card shadow ">
       <div class="row">
        <div class="col-sm-12">
          <br>
          <h2 class="text-center">Admin</h2>
          <hr>
          @if(Auth::user()->foto==null)
            <h2 class="text-center"><img src="{{asset('pic/admin.png')}}" class="rounded-circle" width="200px"></h2>
           @else
            <h2 class="text-center"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" width="200px"></h2>
          @endif
        </div>
      </div>
      <form>
        <div class="form-group row">
          <label for="validationDefault01" class="col-sm-2 col-form-label">Nama</label>
          <div class="col-sm-10">
            <input readonly class="form-control" id="validationDefault01" value="{{Auth::user()->name}}" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="validationDefault01" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
            <input readonly class="form-control" id="validationDefault01" value="{{Auth::user()->email}}" required>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12 text-right">
            <a href="{{ url('/dashboard-admin/update-profile-' . Auth::user()->name) }}" type="button" class="btn btn-primary">
              Edit
            </a>
          </div>
        </div>
      </form>

    </div>

    </div>

  </section>
</div>
@endsection
