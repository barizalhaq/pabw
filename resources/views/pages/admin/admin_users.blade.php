@extends('layouts.profilling')

@section('title')
  Halo Admin
@endsection

@section('content')
  <div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">

    <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
          <div class="col">
            @if(Auth::user()->foto==null)
              <a class="nav-link" href="/dashboard-admin"><img src="{{asset('pic/admin.png')}}" style="width: 50%"></a><br>
            @else
              <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
            @endif
            <h5>Profile</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/verifikasi"><img src="{{asset('pic/verif.png')}}" style="width: 50%"></a><br>
            <h5>Verifikasi</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/statistik"><img src="{{asset('pic/statistik.png')}}" style="width: 50%"></a><br>
            <h5>Statistik</h5>
          </div>
        </div>
        </div>
      </center>
    </div>

    <table class="table">
      <caption>Daftar Verifikasi Kontributor</caption>
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Nama</th>
          <th scope="col">Role</th>
          <th scope="col">Email</th>
          <th scope="col">Jenis Kelamin</th>
          <th scope="col">Alamat</th>
          <th scope="col">No Hp</th>
          <th scope="col">NIK</th>
          <th scope="col">KTP</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
    @foreach($users as $user)
      @if($user->role==0)
        <tbody>
          <tr>
            <th scope="row">{{$user->id}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->role}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->jenis_kelamin}}</td>
            <td>{{$user->alamat}}</td>
            <td>{{$user->no_hp}}</td>
            <td>{{$user->nik}}</td>
            <td> <img src="{{asset('storage/KTP/'.$user->ktp)}}" width="100"> </td>
            <td>
              <form action="/dashboard-admin/stat-users/{{$user->id}}/approve" method="post" onclick="return confirm('Anda yakin untuk Approve?');">
                <button type="submit" class="btn btn-success">Approve</button>
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
              </form>
              <br>
              <form action="/dashboard-admin/stat-users/{{$user->id}}/suspend" method="post" onclick="return confirm('Anda yakin untuk Suspend?');">
                <button type="submit" class="btn btn-warning">Suspend</button>
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
              </form>
            </td>
          </tr>
        </tbody>
        @endif
      @endforeach
      </table>
  </div>
@endsection
