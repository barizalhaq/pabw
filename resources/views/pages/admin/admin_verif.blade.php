@extends('layouts.profilling')

@section('title')

@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
  <section>

    <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
          <div class="col">
            @if(Auth::user()->foto==null)
              <a class="nav-link" href="/dashboard-admin"><img src="{{asset('pic/admin.png')}}" style="width: 50%"></a><br>
            @else
              <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
            @endif
            <h5>Profile</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/verifikasi"><img src="{{asset('pic/verif.png')}}" style="width: 50%"></a><br>
            <h5>Verifikasi</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/statistik"><img src="{{asset('pic/statistik.png')}}" style="width: 50%"></a><br>
            <h5>Statistik</h5>
          </div>
        </div>
        </div>
      </center>
    </div>

  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Verifikasi User</h5>
          <p class="card-text">Verifikasi terhadap user yang melakukan pendaftaran sebagai kontributor.</p>
          <a href="/dashboard-admin/stat-users" class="btn btn-primary">Verifikasi User</a>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Verifikasi Konten</h5>
          <p class="card-text">Verifikasi terhadap konten yang ditulis oleh kontributor.</p>
          <a href="/dashboard-admin/stat-konten" class="btn btn-primary">Verifikasi Konten</a>
        </div>
      </div>
    </div>
  </div>

  </section>
</div>
@endsection
