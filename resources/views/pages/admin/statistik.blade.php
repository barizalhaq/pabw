@extends('layouts.profilling')

@section('header')

@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">
  <section>
    <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
      <center>
        <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
          <div class="row">
          <div class="col">
            @if(Auth::user()->foto==null)
              <a class="nav-link" href="/dashboard-admin"><img src="{{asset('pic/admin.png')}}" style="width: 50%"></a><br>
            @else
              <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
            @endif
            <h5>Profile</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/verifikasi"><img src="{{asset('pic/verif.png')}}" style="width: 50%"></a><br>
            <h5>Verifikasi</h5>
          </div>
          <div class="col">
            <a class="nav-link" href="/dashboard-admin/statistik"><img src="{{asset('pic/statistik.png')}}" style="width: 50%"></a><br>
            <h5>Statistik</h5>
          </div>
        </div>
        </div>
      </center>
    </div>

    <div style="margin-bottom: 2%">
      <div id="app">
        {!! $chart->container() !!}
      </div>
          <script src="{{asset('js/vue.min.js')}}"></script>
          <script>
              var app = new Vue({
                  el: '#app',
              });
          </script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset=utf-8></script>
          {!! $chart->script() !!}
    </div>

    <div style="margin-bottom: 2%">
      <div id="app">
        {!! $chart_2->container() !!}
      </div>
          <script src="{{asset('js/vue.min.js')}}"></script>
          <script>
              var app = new Vue({
                  el: '#app',
              });
          </script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset=utf-8></script>
          {!! $chart_2->script() !!}
    </div>

    <div class="container">
      <table class="table justify-content-center">
        <caption>Daftar Login User</caption>
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Nama</th>
            <th scope="col">Role</th>
            <th scope="col">Tanggal Login</th>
            <th scope="col">Ip Login</th>
          </tr>
        </thead>
      @foreach($users as $user)
          <tbody>
            <tr>
              <th scope="row">{{$user->id}}</th>
              <td>{{$user->name}}</td>
              @if($user->role == 1)
                <td>User</td>
              @elseif($user->role == 2)
                <td>Kontributor</td>
              @elseif($user->role == 3)
                <td>Admin</td>
              @else
                <td>Kontributor(belum approve)</td>
              @endif
              <td>{{$user->last_login_at}}</td>
              <td>{{$user->last_login_ip}}</td>
            </tr>
          </tbody>
        @endforeach
        </table>
    </div>

  </div>

  </section>
@endsection
