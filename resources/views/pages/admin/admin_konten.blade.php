@extends('layouts.profilling')

@section('title')

@endsection

@section('content')
<div class="jumbotron" style="color: #425365; background-color: #ffffff; margin-top: 5%;">

  <div class="jumbotron jumbotron-fluid" style="background-color: #EE7064; margin-top: -40px; min-height: 50%">
    <center>
      <div class="container" style="margin-top: -50px; padding-top: 40px; margin-bottom: -40px ">
        <div class="row">
        <div class="col">
          @if(Auth::user()->foto==null)
            <a class="nav-link" href="/dashboard-admin"><img src="{{asset('pic/admin.png')}}" style="width: 50%"></a><br>
          @else
            <a class="nav-link" href="/profile"><img src="{{asset('storage/profile/' . Auth::user()->foto)}}" class="rounded-circle" style="width: 50%"></a>
          @endif
          <h5>Profile</h5>
        </div>
        <div class="col">
          <a class="nav-link" href="/dashboard-admin/verifikasi"><img src="{{asset('pic/verif.png')}}" style="width: 50%"></a><br>
          <h5>Verifikasi</h5>
        </div>
        <div class="col">
          <a class="nav-link" href="/dashboard-admin/statistik"><img src="{{asset('pic/statistik.png')}}" style="width: 50%"></a><br>
          <h5>Statistik</h5>
        </div>
      </div>
      </div>
    </center>
  </div>

  <table class="table">
    <caption>Daftar Konten</caption>
    <thead>
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Judul</th>
        <th scope="col">Konten</th>
        <th scope="col">Penulis</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
  @foreach($quotes as $quote)
    @if($quote->role==0)
      <tbody>
        <tr>
          <th scope="row">{{$quote->id}}</th>
          <td>{{$quote->title}}</td>
          <td><a href="/artikel/{{$quote->slug}}" class="btn btn-outline-primary">Lihat Konten</a></td>
          <td>{{$quote->user->name}}</td>
          <td>
            <form action="/dashboard-admin/stat-konten/{{$quote->id}}/approve" method="post">
              <button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin untuk menerbitkan konten?');">Approve</button>
              {{csrf_field()}}
              <input type="hidden" name="_method" value="put">
            </form>
            <br>
            <form action="/dashboard-admin/stat-konten/{{$quote->id}}/suspend" method="post" onclick="return confirm('Anda yakin untuk suspend konten?');">
              <button type="submit" class="btn btn-warning">Suspend</button>
              {{csrf_field()}}
              <input type="hidden" name="_method" value="put">
            </form>
          </td>
        </tr>
      </tbody>
      @endif
    @endforeach
    </table>

</div>
@endsection
