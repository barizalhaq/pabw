<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <!-- Required-tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF-token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Text-editor -->
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <!-- Prof-script -->

    <!-- Asset-helper -->
    <base href="{{ URL::asset('/') }}">

    <!-- Bootstrap-CSS -->
    <link rel="stylesheet" href="{{ url('css/pabwproject-style.css') }}">
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Hind|Roboto+Condensed" rel="stylesheet">  

    <!-- Fav-icon -->
    <link rel="icon" href="{{asset('pic/logo.ico')}}">

    <title>@yield('title')</title>
  </head>
  <body>
    @yield('header')
    <nav class="navbar navbar-light navbar-expand-lg fixed-top " style="background-color: #ffffff; padding-left: 90px;
    padding-right: 90px;">

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <a class="navbar-brand" href="/"> <img src="{{asset('pic/logo.png')}}" width="180"> </a>
          <ul class="navbar-nav mr-auto" style="">
            <li class="nav-item">
              <a href="/" style="color: #EE7064" class="nav-link btn-outline-light">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a href="/artikel" style="color: #EE7064" class="nav-link btn-outline-light">Artikel</a>
            </li>
            <li class="nav-item">
              <a href="#kat" style="color: #EE7064" class="nav-link btn-outline-light">Kategori</a>
            </li>
            <li class="nav-item">
              <a href="https://twitter.com/bnpb_indonesia" style="color: #EE7064" class="nav-link btn-outline-light">Kontak</a>
            </li class="nav-item">
          </ul>

          @if (Route::has('login'))
            @auth
            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #ffffff; height:65px !important">
                @if(Auth::user()->foto==null)
                  @if(Auth::user()->jenis_kelamin=='laki')
                    <img src="{{asset('pic/cowo.png')}}" height="55px" class="rounded-circle">
                    @else
                      <img src="{{asset('pic/cewe.png')}}" height="55px" class="rounded-circle">
                  @endif
                @else
                  <img src="{{asset('storage/profile/' . Auth::user()->foto)}}" height="55px" class="rounded-circle">
                @endif
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @if (Auth::user()->role==2 || Auth::user()->role==0)
                  <a class="dropdown-item btn-outline-light" style="color: #EE7064" href="{{ url('/dashboard') }}">Dashboard</a>
                @elseif(Auth::user()->role==3)
                  <a class="dropdown-item btn-outline-light" style="color: #EE7064" href="{{ url('/dashboard-admin') }}">Dashboard</a>
                @else
                  <a class="dropdown-item btn-outline-light" style="color: #EE7064" href="{{ url('/profile') }}">Profile</a>
                @endif
                    <a style="color: #EE7064" class="dropdown-item btn-outline-light" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                      Logout
                    </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
            </div>

          @else
          <div class="top-right links">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a style="color: #EE7064" class="nav-link btn-outline-light" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a style="color: #EE7064" class="nav-link btn-outline-light" href="{{ route('register') }}">Register</a>
              </li>
            </ul>
          </div>
            @endauth
          @endif
      </div>
    </nav>

      @yield('content')

    <footer id="ten">
      <center>
        <div class="container" style="margin-bottom: 30px">

          <div class="row">
          <div class="col">

          </div>
          <div id="kat" class="col">
            <img src="{{asset('pic/setting.png')}}" style="width: 50px; "><br>
            <p style="margin-top: -5px;  color: #fff">Tentang</p>
          </div>
          <div class="col">

          </div>
        </div>

        </div>
    </center>
    <div class="container">
      <div class="row" >
        <div class="col-sm-5 offset-sm-1">
          <table>
            <tr>
              <td style=""><img src="{{asset('pic/email.png')}}" style="width: 30px;"></td>
              <td style="padding-left: 5px;  color: #fff">
                17523051@students.uii.ac.id
                <br>
                17523181@students.uii.ac.id
                <br>
                17523183@students.uii.ac.id
              </td>
            </tr>
            <tr>
              <td style="padding-top: 20px"><img src="{{asset('pic/phone.png')}}" style="width: 30px;"></td>
              <td style="padding-left: 5px; padding-top: 20px; color: #fff ;margin-top: 20px">
                081292278689
                <br>
                085707647239
              </td>
            </tr>
            <tr>
              <td style="padding-top: 20px"><img src="{{asset('pic/lokasi.png')}}" style="width: 30px;"></td>
              <td style="padding-left: 5px; padding-top: 20px; color: #fff ;margin-top: 20px">
                <a href="https://goo.gl/maps/oPHgh9tu4B12" target="_blank">
                  <p style="color: #fff; text-decoration: none;">Faculty of Industrial Technology UII</p>
                </a>
              </td>
            </tr>
          </table>
        </div>
        <div class="col-sm-5">
          <h4>Temukan kami di :</h4>
          <a href=""><img src="{{asset('pic/fb.png')}}" class="rounded-circle" width="30px"></a>
          <a href=""><img src="{{asset('pic/ig.png')}}" class="rounded-circle" width="35px"></a>
          <a href=""><img src="{{asset('pic/tw.png')}}" class="rounded-circle" width="32px"></a>
        </div>
      </div>

      <div class="row text-center">
        <div class="col-sm-12">
        <hr>
          <p>
            &copy; copyright 2018 DoaBapak & Laravel
          </p>
        </div>
      </div>
    </div>
  </footer>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    });
    $('#myCollapsible').on('hidden.bs.collapse', function () {
// do something…
});
  </script>



</body>
</html>
